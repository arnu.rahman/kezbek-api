import { RedisModuleOptions } from '@liaoliaots/nestjs-redis';
import * as dotenv from 'dotenv'
dotenv.config();

export const redisModuleOptions: RedisModuleOptions = {
  config: {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT),
  },
};

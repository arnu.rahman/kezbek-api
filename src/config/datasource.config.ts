import { DataSource, DataSourceOptions } from "typeorm";
import * as dotenv from 'dotenv'
dotenv.config();

export const dataSourceOptions: DataSourceOptions = {
    type: 'postgres',
    host: process.env.PG_HOST,
    port: parseInt(process.env.PG_PORT),
    username: process.env.PG_USERNAME,
    password: process.env.PG_PASSWORD,
    database:  process.env.PG_DATABASE,
    entities: [__dirname + '/../**/*.entity.{js,ts}'],
    synchronize: true,
    logging: false,
}

const dataSource = new DataSource(dataSourceOptions);
dataSource.initialize();
export default dataSource;
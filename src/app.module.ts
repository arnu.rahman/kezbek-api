import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { dataSourceOptions } from './config/datasource.config';
import { RedisModule } from '@liaoliaots/nestjs-redis';
import { redisModuleOptions } from './config/redis.config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TransactionModule } from './transaction/transaction.module';
import { PageService } from './etc/service/page/page.service';

@Module({
  imports: [ 
    ConfigModule.forRoot({ isGlobal: true}),
    TypeOrmModule.forRoot(dataSourceOptions),
    RedisModule.forRoot(redisModuleOptions),
    TransactionModule,
  ],
  controllers: [AppController],
  providers: [AppService,PageService],
})
export class AppModule {}

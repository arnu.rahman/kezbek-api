import { Injectable } from '@nestjs/common';
import { FindManyOptions, Like } from 'typeorm';
import * as moment from 'moment';

@Injectable()
export class PageService {
    async generatePage(data, repo, opt: FindManyOptions = {}) {
        let { page, limit, ...where } = data
        if (where) {
            let filter = {}
            Object.keys(where).forEach(element => {
                filter[element] = Like(`%${where[element]}%`)
            })
            opt.where = filter
        }

        let total = await repo.count(opt)
        opt.skip = (data.page - 1) * data.limit
        opt.take = data.limit

        let result = await repo.find(opt)
        let pages = Math.ceil(total / data.limit)
        let finalData = {
            total: total,
            page: data.page,
            pages: pages,
            data: await this.formattedResult(result)
        }

        return finalData
    }

    async formattedResult(result) {
        let Obj = []
        for (const element of result) {
            let temp = {
                id: element.id,
                trx_date: element.nik,
                customer_email: element.customer_email,
                partner_id: element.partner_id,
                promo_code: element.promo_code,
                quantity: element.quantity,
                checkout_total: element.checkout_total,
                cashback_trx: element.cashback_trx,
                cashback_loyalty: element.cashback_loyalty,
                cashback_total: element.cashback_total,
                create_at: (element.create_at) ? moment(element.create_at).format('YYYY-MM-DD HH:mm:ss') : null,
                update_at: (element.update_at) ? moment(element.update_at).format('YYYY-MM-DD HH:mm:ss') : null,
                delete_at: (element.delete_at) ? moment(element.delete_at).format('YYYY-MM-DD HH:mm:ss') : null,
            }
            Obj.push(temp);
        }

        return Obj
    }
}

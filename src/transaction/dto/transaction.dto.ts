import { ApiProperty } from "@nestjs/swagger/dist/decorators"
import { Type } from "class-transformer";
import { IsOptional,IsEmail, IsString, MaxLength, IsNotEmpty, IsDate } from "class-validator"

export class TransactionDto {
    @IsOptional()
    id?: number

    @ApiProperty({required: true})
    @IsDate()
    @Type(() => Date)
    trx_date: string;

    @ApiProperty({required: true})
    @IsString()
    @MaxLength(254)
    @IsEmail()
    customer_email: string;

    @ApiProperty({required: true})
    @IsNotEmpty()
    partner_id: number;

    @ApiProperty({required: false})
    @IsString()
    @MaxLength(10)
    promo_code: string;

    @ApiProperty({required: true})
    @IsNotEmpty()
    qty: number;

    @ApiProperty({required: true})
    @IsNotEmpty()
    checkout_total: number;

    @ApiProperty({required: false})
    @IsOptional()
    cashback_trx: number;

    @ApiProperty({required: false})
    @IsOptional()
    cashback_loyalty: number;

    @ApiProperty({required: false})
    @IsOptional()
    cashback_total: number;

    @ApiProperty({required: false})
    @IsOptional()
    create_at: string

    @ApiProperty({required: false})
    @IsOptional()
    update_at: string

    @ApiProperty({required: false})
    @IsOptional()
    delete_at: string
}
import { PageRequestDto } from "src/etc/dto/page-dto";
import { ApiProperty } from "@nestjs/swagger/dist/decorators"
import { IsString, IsNotEmpty, MinLength, IsEmail  } from "class-validator"

export class RequestTransactionDto extends PageRequestDto{
    @ApiProperty({required:true})
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    @MinLength(5)
    customer_email: string;
}
import { PageResponseDto } from "src/etc/dto/page-dto";
import { ApiProperty } from "@nestjs/swagger/dist/decorators"
import { TransactionDto } from "./transaction.dto";

export class ResponseTransactionDto {
    @ApiProperty({type:[TransactionDto]})
    data: TransactionDto
}
import { Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Transaction {
    @PrimaryGeneratedColumn({ primaryKeyConstraintName: "pk_trx_id" })
    id: number

    @Column({ type: 'date' })
    trx_date: string;

    @Column({ type: "varchar", length: 254})
    customer_email: string;

    @Column()
    partner_id: number;

    @Column({ type: "varchar", length: 20})
    promo_code: string;

    @Column()
    qty: number;

    @Column()
    checkout_total: number;

    @Column()
    cashback_trx: number;

    @Column()
    cashback_loyalty: number;

    @Column()
    cashback_total: number;
    
    @CreateDateColumn({ type: 'timestamp' })
    create_at: string

    @UpdateDateColumn({ type: 'timestamp' })
    update_at: string

    @DeleteDateColumn({ type: 'timestamp' })
    delete_at: string
}

import { Controller, Get, Post, Body, Headers, Query } from '@nestjs/common';
import { TransactionService } from './transaction.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ResponseTransactionDto } from './dto/response-transaction.dto';
import { RequestTransactionDto } from './dto/request-transaction.dto';

@ApiTags('Transaction')
@Controller('transaction')
export class TransactionController {
  constructor(private readonly transactionService: TransactionService) {}

  @Post()
  create(@Headers('token') token: string, @Body() createTransactionDto: CreateTransactionDto) {
    return this.transactionService.create(token, createTransactionDto);
  }

  @Get()
  @ApiOkResponse({type: ResponseTransactionDto})
  findAll(@Headers('token') token: string, @Query() page: RequestTransactionDto) {
    return this.transactionService.findAll(token,page);
  }
}

import { Injectable, Logger } from '@nestjs/common';
import { PageService } from 'src/etc/service/page/page.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { TransactionRepository } from './repository/transaction.repository';

@Injectable()
export class TransactionService  extends PageService {
  private readonly logger = new Logger(TransactionService.name);

  constructor(
    private readonly transRepository: TransactionRepository
  ) {
    super()
  }

  create(token,createTransactionDto: CreateTransactionDto) {
    return this.transRepository.save(createTransactionDto);
  }

  findAll(token,filter) {
    //encode token and check
    return this.generatePage(filter, this.transRepository, { order: { update_at: "DESC" } })
  }
}
